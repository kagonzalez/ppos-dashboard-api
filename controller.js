var Promise = require('bluebird');



var Controller = function(){};


Controller.prototype.getConnection = function(pool){
    var self = this;
    return new Promise(function(resolve, reject) {
        try {
            pool.getConnection(function (err, connection) {
                if (err) {
                    reject({"http_code": 500, "code": 841, "message": "Error in connection database"});
                }
                else {
                    resolve(connection);
                }
            });
        }catch(e){
            reject({"code": 841, "message": "Error in connection database"});
        }
    });
};

Controller.prototype.getTransactionTypes=function(pool){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from transaction_types", function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code": 500, "code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.insertTermsConditions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("insert into term_conditions set ? ", {
                    store: data.store,
                    transaction_type_id: data.transaction_type_id,
                    text: data.text
                }, function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.createSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("insert into sessions set ? ", {
                    store: data.store,
                    user: data.user,
                    uuid: data.uuid,
                    model: data.model,
                    token: data.token
                }, function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.isSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select count(*) as c from sessions where token=?", [data.token], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.getSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from sessions where token=?", [data.token], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};


Controller.prototype.getUserSessions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from sessions where store=? and user=?", [data.store, data.user], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.deleteSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("delete from sessions where token=? and store=? and user=?", [data.token, data.store, data.user], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};



module.exports = new Controller();
